package com.zhlab.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName TestController
 * @Description //测试控制器
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2021/1/7 0007 下午 4:28
 **/
@Controller
public class TestController {

    @GetMapping("/index")
    public String index(){
        return "index.html";
    }
}

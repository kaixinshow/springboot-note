package com.zhlab.demo.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zhlab.demo.encrypt.annotation.Decrypt;
import com.zhlab.demo.encrypt.annotation.Encrypt;
import com.zhlab.demo.encrypt.utils.AesEncryptUtil;
import com.zhlab.demo.model.SysAdminUser;
import com.zhlab.demo.model.UserDto;
import com.zhlab.demo.service.SysAdminUserService;
import com.zhlab.demo.utils.PageUtil;
import com.zhlab.demo.utils.page.PageRequest;
import com.zhlab.demo.utils.page.PageResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName UserController
 * @Description //用户接口层
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/10/31 0031 上午 9:43
 **/
@RestController
@RequestMapping("/user")
public class UserController {

    private static final String KEY = "abcdef0123456780";

    @Autowired
    SysAdminUserService sysAdminUserService;


    /* 方法注解 */
    @ApiOperation(value = "方法名:用户列表", notes = "获取用户列表")
    @GetMapping("/list")
    public List<SysAdminUser> list(){
        List<SysAdminUser> list = sysAdminUserService.findAll();

        return list;
    }

    /* 方法注解 */
    @ApiOperation(value = "方法名:用户列表2", notes = "切换数据源获取用户列表")
    @GetMapping("/list2")
    public List<SysAdminUser> list2(){
        List<SysAdminUser> list = sysAdminUserService.findAll2();

        return list;
    }

    @ApiOperation(value = "方法名:用户列表分页", notes = "获取分页用户列表")
    @PostMapping(value="/findPage")
    public PageResult findPage(@RequestBody PageRequest pageRequest) {

        startPage(pageRequest); //开启分页,pageHelp会自动拦截mybatis的sql
        List<SysAdminUser> list = sysAdminUserService.findAll();
        return PageUtil.getPageResult(new PageInfo<>(list));
    }


    /**
     * 启动分页
     * */
    private void startPage(PageRequest pageRequest){
        PageHelper.startPage(pageRequest.getPageNum() == 0?1:pageRequest.getPageNum(),
                pageRequest.getPageSize()==0?5:pageRequest.getPageSize());
    }


    /**
     * get请求->一个或多个参数使用工具类解密
     *
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    @Encrypt
    @GetMapping("/getStrData")
    public String getStrData(String username, String password) throws Exception {
        username = AesEncryptUtil.aesDecrypt(username, KEY);
        password = AesEncryptUtil.aesDecrypt(password, KEY);
        System.out.println(username + "---" + password);

        return "加密字符串";
    }

    /**
     * post请求->一个或多个参数使用工具类解密
     * @param username
     * @param password
     * @return
     * @throws Exception
     */
    @Encrypt
    @PostMapping("/postStrData")
    public String postStrData(String username, String password) throws Exception {
        username = AesEncryptUtil.aesDecrypt(username, KEY);
        password = AesEncryptUtil.aesDecrypt(password, KEY);
        System.out.println(username + "---" + password);

        return "加密字符串";
    }

    /**
     * post请求->一个参数使用@Decrypt注解解密
     *
     * @param username
     * @return
     * @throws Exception
     */
    @Encrypt
    @Decrypt
    @PostMapping("/postStrData2")
    public String postStrData2(@RequestBody String username) throws Exception {
        System.out.println("username: " + username);

        return "加密字符串";
    }

    /**
     * post请求->多个参数使用@Decrypt注解解密
     *
     * @param paramsMap
     * @return
     * @throws Exception
     */
    @Encrypt
    @Decrypt
    @PostMapping("/postStrData3")
    public String postStrData4(@RequestBody Map<String, Object> paramsMap) throws Exception {
        System.out.println("username: " + paramsMap);
        System.out.println(paramsMap.get("username") + "---" + paramsMap.get("password"));

        return "加密字符串";
    }

    /**
     * post请求->发送并获取实体类数据,使用@Decrypt注解解密
     */
    @Encrypt
    @Decrypt
    @PostMapping("/encryptDto")
    public UserDto encryptDto(@RequestBody UserDto dto) {
        System.err.println(dto.getId() + "\t" + dto.getName());
        return dto;
    }

    /**
     * post请求->发送并获取map数据,使用@Decrypt注解解密
     *
     * @return
     */
    @Encrypt
    @Decrypt
    @PostMapping("/encryptMap")
    public Map<String, Object> encryptMap(@RequestBody Map<String, Object> map) {
        String username = (String) map.get("username");
        int password = (int) map.get("password");
        System.out.println(username + "---" + password);

        Map<String, Object> resultMap = new HashMap<>();

        UserDto dto = new UserDto();
        dto.setId(1);
        dto.setName("加密实体对象");

        UserDto dto2 = new UserDto();
        dto2.setId(2);
        dto2.setName("加密实体对象2");

        List<UserDto> list = new ArrayList<>();
        list.add(dto);
        list.add(dto2);

        resultMap.put("int", 666);
        resultMap.put("String", "singleZhang");
        resultMap.put("dto", dto);
        resultMap.put("list", list);

        return resultMap;
    }

}

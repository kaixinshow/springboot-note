package com.zhlab.demo.encrypt.algorithm;


import com.zhlab.demo.encrypt.utils.AesEncryptUtil;

/**
 * Aes加密算法实现
 */
public class AesEncryptAlgorithm implements EncryptAlgorithm {

    @Override
    public String encrypt(String content, String encryptKey) throws Exception {
        return AesEncryptUtil.aesEncrypt(content, encryptKey);
    }

    @Override
    public String decrypt(String encryptStr, String decryptKey) throws Exception {
        return AesEncryptUtil.aesDecrypt(encryptStr, decryptKey);
    }

}

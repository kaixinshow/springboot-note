package com.zhlab.demo.encrypt.annotation;

import java.lang.annotation.*;

/**
 * 解密注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Decrypt {

    String value() default "";
}

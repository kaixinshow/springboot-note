package org.zhangsan.zookeeper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zhangsan.beans.ZookeeperClient;

/**
 * @ClassName ZkIdMakerTest
 * @Description //分布式ID生成测试
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/8 0008 下午 5:18
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ZkIdMakerTest {
    @Autowired
    private ZookeeperClient zookeeperClient;

    @Test
    public void zookeeperIdMakerTest(){
        String nodeName = "/test/zkIdMaker/ID-";
        for (int i = 0; i < 10; i++) {
            String id = zookeeperClient.makeId(nodeName);
            System.out.println("第" + i + "个创建的id为:" + id);
        }
    }
}

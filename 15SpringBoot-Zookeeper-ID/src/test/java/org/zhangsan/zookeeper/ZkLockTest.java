package org.zhangsan.zookeeper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.zhangsan.beans.ZookeeperClient;
import org.zhangsan.beans.lock.TestLock;

/**
 * @ClassName ZkLockTest
 * @Description //Zookeeper锁测试
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/8 0008 下午 3:30
 **/
@RunWith(SpringRunner.class)
@SpringBootTest
public class ZkLockTest {

    @Autowired
    private ZookeeperClient zookeeperClient;

    @Test
    public void zookeeperLockTest(){
        String lockId = "123123";
        String result = zookeeperClient.lock(new TestLock<String>(lockId) {
            @Override
            public String execute() {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return this.getLockId();
            }
        });

        if (result == null) {
            System.out.println("执行失败");
        } else {
            System.out.println("执行成功");
        }
    }
}

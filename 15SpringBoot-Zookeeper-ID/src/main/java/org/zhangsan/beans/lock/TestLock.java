package org.zhangsan.beans.lock;

import lombok.Getter;

/**
 * @ClassName TestLock
 * @Description //lock 测试类
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/8 0008 下午 3:26
 **/
public abstract class TestLock<String> extends AbstractZookeeperLock<String> {

    private static final java.lang.String LOCK_PATH = "test_";

    @Getter
    private String lockId;

    public TestLock(String lockId) {
        this.lockId = lockId;
    }

    @Override
    public java.lang.String getLockPath() {
        return LOCK_PATH + this.lockId;
    }
}

package org.zhangsan.beans.lock;

import java.util.concurrent.TimeUnit;

/**
 * @ClassName AbstractZookeeperLock
 * @Description //AbstractZookeeperLock 锁
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/8 0008 下午 3:17
 **/
public abstract class AbstractZookeeperLock<T> {
    private static final int TIME_OUT = 5;

    public abstract String getLockPath();

    public abstract T execute();

    public int getTimeout(){
        return TIME_OUT;
    }

    public TimeUnit getTimeUnit(){
        return TimeUnit.SECONDS;
    }
}

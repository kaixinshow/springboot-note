package org.zhangsan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
public class ZkBootApplication {
    public static void main(String[] args){
        SpringApplication.run(ZkBootApplication.class,args);
    }
}

package org.zhangsan.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.zhangsan.beans.ZookeeperClient;

/**
 * @ClassName ZookeeperConfig
 * @Description //Zookeeper 配置
 * @Author singleZhang
 * @Email 405780096@qq.com
 * @Date 2020/12/8 0008 下午 2:52
 **/
@Configuration
@PropertySource("classpath:config/zookeeper.properties")
public class ZookeeperConfig {

    @Autowired
    private Environment environment;

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public ZookeeperClient zookeeperClient(){
        String zookeeperServer = environment.getRequiredProperty("zookeeper.server");
        String zookeeperLockPath = environment.getRequiredProperty("zookeeper.lockPath");
        return new ZookeeperClient(zookeeperServer, zookeeperLockPath);
    }

}
